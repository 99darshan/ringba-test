using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ringba_test
{
    static class StringUtil
    {
        /// <summary>
        /// splits Camel Case String in to an string array of words.
        /// </summary>
        /// <param name="camelCaseString"></param>
        /// <returns></returns>
        internal static string[] SplitCamelCaseString (string camelCaseString)
        {
            string[] splitArr = Regex.Split (camelCaseString, @"(?<!^)(?=[A-Z])");
            return splitArr;
        }

        /// <summary>
        /// Prints most common two character prefix from an string array
        /// </summary>
        /// <param name="wordsArr"></param>
        internal static void CommonTwoCharPrefix (string[] wordsArr)
        {
            Dictionary<string, int> prefixCountDict = new Dictionary<string, int> ();
            foreach (string word in wordsArr)
            {
                if (word.Length <= 2) continue;
                string prefix = word.Substring (0, 2).ToLower ();
                if (prefixCountDict.ContainsKey (prefix)) prefixCountDict[prefix] += 1;
                else prefixCountDict.Add (prefix, 1);

            }
            string maxCountItem = "";
            int maxCount = 0;
            foreach (KeyValuePair<string, int> kv in prefixCountDict)
            {
                if (kv.Value > maxCount)
                {
                    maxCount = kv.Value;
                    maxCountItem = kv.Key;
                }
            }

            Console.WriteLine ($"The most common two character prefix is {maxCountItem.ToUpper()} and it occurred {maxCount} times.");
        }

        /// <summary>
        /// Prints the Frequency of each character in a string 
        /// treats upper case and lower case letters as different
        /// </summary>
        /// <param name="str"></param>
        internal static void FrequencyOfEachCharacter (string str)
        {
            Dictionary<char, int> letterCountDict = new Dictionary<char, int> ();
            foreach (char c in str)
            {
                if (letterCountDict.ContainsKey (c)) letterCountDict[c] += 1;
                else letterCountDict.Add (c, 1);
            }

            foreach (KeyValuePair<char, int> kv in letterCountDict)
            {
                Console.WriteLine ($"Occurrance of Letter {kv.Key} : {kv.Value}");
            }
        }

        /// <summary>
        /// Prints the most common word 
        /// </summary>
        /// <param name="wordsArr"></param>
        internal static void MostCommonWord (string[] wordsArr)
        {
            Dictionary<string, int> wordCountDict = new Dictionary<string, int> ();
            foreach (string item in wordsArr)
            {
                if (wordCountDict.ContainsKey (item))
                {
                    wordCountDict[item] += 1;

                }
                else
                {
                    wordCountDict.Add (item, 1);
                }
            }
            string maxCountWord = "";
            int maxCount = 0;
            foreach (KeyValuePair<string, int> kv in wordCountDict)
            {
                if (kv.Value > maxCount)
                {
                    maxCount = kv.Value;
                    maxCountWord = kv.Key;
                }
            }
            Console.WriteLine ($"Most common word is {maxCountWord} and it occured {maxCount} times");
        }

    }
}