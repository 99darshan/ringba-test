using System;
using System.IO;
using System.Net;

namespace ringba_test
{
    static class IOUtil
    {
        internal static void DownloadFile (string url, string filePath)
        {
            using (WebClient client = new WebClient ())
            {
                try
                {
                    if (!File.Exists (filePath))
                    {
                        client.DownloadFile (url, filePath);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine (e.Message);
                }
            }
        }
    }
}