﻿using System;
using System.IO;

namespace ringba_test
{
    class Program
    {
        static void Main (string[] args)
        {
            const string url = "https://ringba-test-html.s3-us-west-1.amazonaws.com/TestQuestions/output.txt";
            string filePath = Path.Join (Directory.GetCurrentDirectory (), "CamelCaseStringFile.txt");
            // 1. Download File
            IOUtil.DownloadFile (url, filePath);
            string camelCaseString = File.ReadAllText (filePath);
            string[] parsedString = StringUtil.SplitCamelCaseString (camelCaseString);

            //Console.WriteLine ($"Parsed String: {String.Join(' ', parsedString)}");

            // 1. Count of each letters in the file, upper case and lower case letters are not treated as same here
            StringUtil.FrequencyOfEachCharacter (camelCaseString);

            Console.WriteLine ("*************************************");

            // 2. Count of Uppercase letters
            Console.WriteLine ($"Count of Capitalized letters: {parsedString.Length}");

            Console.WriteLine ("*************************************");

            // 3. Most Commoun Word and its Count 
            StringUtil.MostCommonWord (parsedString);

            Console.WriteLine ("*************************************");

            // 4. Most Common Two Char Prefix and its Count
            StringUtil.CommonTwoCharPrefix (parsedString);

            Console.WriteLine ("*************************************");

        }
    }
}